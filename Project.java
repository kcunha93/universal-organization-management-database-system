
import java.sql.*;
import java.util.Scanner;


public class Project {
	
	
	
public static void main(String[] args) throws ClassNotFoundException, SQLException {
// TODO Auto-generated method stub
	
Scanner input = new Scanner(System.in);


int selection;
System.out.println("Welcome to The Club Database!");
System.out.println("Please select your member type:\n");
System.out.print("Potential member(1) \nClub Advisor (2)\nClub Member (3)\nPresident or Vice President(4)\nEvent Planner(5)\nTreasurer(6)\n");


selection = input.nextInt();
if(selection==1)
PotMem();

else if (selection==2)
Advisor();

else if (selection==3)
Member();

else if (selection==4)
President();

else if (selection==5)
EventPlanner();

else if (selection==6)
Treasurer();
} 


public static void PotMem () throws SQLException, ClassNotFoundException{
Connection conn = null;
Statement stat = null;
Class.forName("org.sqlite.JDBC");


Scanner input = new Scanner(System.in);


/*String password;
System.out.print("Password:");

password = input.nextLine();*/

/*if (password != "president")//can change password depending on the user
return;*/

//else{
	
int selection2 = 1;
String query = null;//had to initialize as null to fix an error


while (selection2 != 0)
{
	System.out.println("Select what you would like to do:\n");
	System.out.print("See Club Information(1) \nView Club Events(2)\n");
	
	System.out.print("Exit (0)\n");//Put in options for all of the use cases
	
	
	selection2 = input.nextInt();
	if (selection2 == 1){
		
		query = "Select cl_name, cl_description, cl_contacts From Club_Info";//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
	
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			ResultSet rs = stat.executeQuery(query);
			

	        while(rs.next()){
	           //Retrieve by column name
	           String cl_name  = rs.getString("cl_name");
	           
	           String descript = rs.getString("cl_description");
	           
	           String cl_contact = rs.getString("cl_contacts");
	          

	           //Display values
	           System.out.print("Club Name: " + cl_name + "\n");
	           System.out.print("Description: " + descript + "\n");
	           System.out.print("Club Contact Member: " + cl_contact + "\n\n");
	           
			
	        }
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
		
	}
	
	
	
	
	
	
	else if (selection2 == 2){
		query=" Select * From Events";
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			ResultSet rs = stat.executeQuery(query);
			

	        while(rs.next()){
	           //Retrieve by column name
	           String e_name  = rs.getString("e_name");
	           
	           String location = rs.getString("e_location");
	           
	           String time = rs.getString("e_when");
	          

	           //Display values
	           System.out.print("Event Name: " + e_name + "\n");
	           System.out.print("Location: " + location + "\n");
	           System.out.print("Time of Event: " + time + "\n\n");
	           
			
	        }
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
		
	}
	

	
	
	
System.out.print("Would you like to do anything else? Yes(1) or No(0)\n");
selection2= input.nextInt();
}
return;
}

public static void Advisor () throws SQLException, ClassNotFoundException{
Connection conn = null;
Statement stat = null;
Class.forName("org.sqlite.JDBC");


Scanner input = new Scanner(System.in);

String password;
System.out.print("Password:");

password = input.nextLine();
if (password.equals("advisor") == false )//can change password depending on the user
return;

else{
	
int selection2 = 1;
String query = null;//had to initialize as null to fix an error


while (selection2 != 0)
{
	System.out.println("Select what you would like to do:\n");
	System.out.print("View Club Events(1)\nView Contacts(2)\nView Budgets(3)\n");
	
	System.out.print("Exit (0)\n");//Put in options for all of the use cases
	
	
	selection2 = input.nextInt();
	
	if (selection2 == 1){
		query=" Select * From Events";
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			ResultSet rs = stat.executeQuery(query);
			

	        while(rs.next()){
	           //Retrieve by column name
	           String e_name  = rs.getString("e_name");
	           
	           String location = rs.getString("e_location");
	           
	           String time = rs.getString("e_when");
	          

	           //Display values
	           System.out.print("Event Name: " + e_name + "\n");
	           System.out.print("Location: " + location + "\n");
	           System.out.print("Time of Event: " + time + "\n\n");
	           
			
	        }
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
		
	}
	else if (selection2 == 2){
		
		query = "Select c_name, c_phonenum, c_email,c_position From Contact_List";//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
	
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			ResultSet rs = stat.executeQuery(query);
			

	        while(rs.next()){
	           //Retrieve by column name
	           String c_name  = rs.getString("c_name");
	           
	           String phonenum = rs.getString("c_phonenum");
	           
	           String email = rs.getString("c_email");
	          
			   String position = rs.getString("c_position");

	           //Display values
	           System.out.print("Member Name: " + c_name + "\n");
	           System.out.print("Phone Number: " + phonenum + "\n");
	           System.out.print("email: " + email + "\n");
	           System.out.print("Position: " + position + "\n\n");
			
	        }
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
		
	}
	else if (selection2 == 3){
		System.out.print("All budgets(1)\n One month(2)");
		int selection3=input.nextInt();
		
		if(selection3 == 1) {
		query = "Select * FROM Accounts";//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
	
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			ResultSet rs = stat.executeQuery(query);
			

	        while(rs.next()){
	           //Retrieve by column name
	           String category  = rs.getString("a_category");
	           
	           String balance = rs.getString("a_balance");
	           
	           String month = rs.getString("a_month");

	           //Display values
	           System.out.print("Category: " + category + "\n");
	           System.out.print("Balance: " + balance + "\n");
	           System.out.print("Month: " + month + "\n\n");
			
	        }
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
	}	
	
		else if(selection3 == 2) {
			String dummy = input.nextLine();
			System.out.print("Please enter month in the form YYYY-MM:");
			String inMonth=input.nextLine();
			query = "Select * FROM Accounts WHERE a_month LIKE '"+inMonth+"%'";//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
	
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			ResultSet rs = stat.executeQuery(query);
			

	        while(rs.next()){
	           //Retrieve by column name
	           String category  = rs.getString("a_category");
	           
	           String balance = rs.getString("a_balance");
	           
	           String month = rs.getString("a_month");

	           //Display values
	           System.out.print("Category: " + category + "\n");
	           System.out.print("Balance: " + balance + "\n");
	           System.out.print("Month: " + month + "\n\n");
			
	        }
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
	}	
}

	
	
	
System.out.print("Would you like to do anything else? Yes(1) or No(0)\n");
selection2= input.nextInt();
}
return;
}



}



public static void  Member() throws SQLException, ClassNotFoundException{
Connection conn = null;
Statement stat = null;
Class.forName("org.sqlite.JDBC");


Scanner input = new Scanner(System.in);


String password;
System.out.print("Password:");

password = input.nextLine();

if (password.equals("member") == false )//can change password depending on the user
return;

else{
	
int selection2 = 1;
String query = null;//had to initialize as null to fix an error


while (selection2 != 0)
{
	System.out.println("Select what you would like to do:\n");
	System.out.println("See Club Information(1) \nView Club Events(2)\nView Future Events(3) \nView Contact List(4)\nCheck Dues Payment(5)\nCheck Status(6)");
	
	System.out.print("Exit (0)\n");//Put in options for all of the use cases
	
	
	selection2 = input.nextInt();
	if (selection2 == 1){
		
		query = "Select cl_name, cl_description, cl_contacts From Club_Info";//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
	
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			ResultSet rs = stat.executeQuery(query);
			

	        while(rs.next()){
	           //Retrieve by column name
	           String cl_name  = rs.getString("cl_name");
	           
	           String descript = rs.getString("cl_description");
	           
	           String cl_contact = rs.getString("cl_contacts");
	          

	           //Display values
	           System.out.print("Club Name: " + cl_name + "\n");
	           System.out.print("Description: " + descript + "\n");
	           System.out.print("Club Contact Member: " + cl_contact + "\n\n");
	           
			
	        }
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
		
	}
	else if (selection2 == 2){
		query=" Select * From Events";
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			ResultSet rs = stat.executeQuery(query);
			

	        while(rs.next()){
	           //Retrieve by column name
	           String e_name  = rs.getString("e_name");
	           
	           String location = rs.getString("e_location");
	           
	           String time = rs.getString("e_when");
	          

	           //Display values
	           System.out.print("Event Name: " + e_name + "\n");
	           System.out.print("Location: " + location + "\n");
	           System.out.print("Time of Event: " + time + "\n\n");
	           
			
	        }
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
		
	}
	
	else if (selection2 == 3){
		query=" Select * From Events Where e_when > date('now')";
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			ResultSet rs = stat.executeQuery(query);
			

	        while(rs.next()){
	           //Retrieve by column name
	           String e_name  = rs.getString("e_name");
	           
	           String location = rs.getString("e_location");
	           
	           String time = rs.getString("e_when");
	          

	           //Display values
	           System.out.print("Event Name: " + e_name + "\n");
	           System.out.print("Location: " + location + "\n");
	           System.out.print("Time of Event: " + time + "\n\n");
	           
			
	        }
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
		
	}
	else if (selection2 == 4){
		
		query = "Select c_name, c_phonenum, c_email,c_position From Contact_List";//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
	
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			ResultSet rs = stat.executeQuery(query);
			

	        while(rs.next()){
	           //Retrieve by column name
	           String c_name  = rs.getString("c_name");
	           
	           String phonenum = rs.getString("c_phonenum");
	           
	           String email = rs.getString("c_email");
	          
			   String position = rs.getString("c_position");

	           //Display values
	           System.out.print("Member Name: " + c_name + "\n");
	           System.out.print("Phone Number: " + phonenum + "\n");
	           System.out.print("email: " + email + "\n");
	           System.out.print("Position: " + position + "\n\n");
			
	        }
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
		
	}
	else if (selection2 == 5){
		System.out.print("Enter your id number:");
		int myID= input.nextInt();
		query = "Select d_amount From DUES Where d_id ="+ myID;//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
	
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			ResultSet rs = stat.executeQuery(query);
			

	        while(rs.next()){
	           //Retrieve by column name
	           String amount  = rs.getString("d_amount");
	          
			  //Display values
	           System.out.print("ID number: " + myID + "\n");
	           System.out.print("Amount Paid: " + amount + "\n\n");
			
	        }
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
		
	}
	else if (selection2 == 6){
		System.out.print("Enter your id number:");
		int myID=input.nextInt();
		query = "Select s_dues,s_part From Status Where s_id ="+ myID;//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
	
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			ResultSet rs = stat.executeQuery(query);
			

	        while(rs.next()){
	           //Retrieve by column name
	           String dues  = rs.getString("s_dues");
			   String part  = rs.getString("s_part");
	          
			   
			   
			  //Display values
	           System.out.print("ID number: " + myID + "\n");
	           System.out.print("Amount Paid: " + dues + "\n");
			   System.out.print("Participation req.: " + part + "\n\n");
			
	        }
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
		
	}
	
	
	
System.out.print("Would you like to do anything else? Yes(1) or No(0)\n");
selection2= input.nextInt();
}
return;
}

}
public static void President () throws SQLException, ClassNotFoundException{
Connection conn = null;
Statement stat = null;
Class.forName("org.sqlite.JDBC");


Scanner input = new Scanner(System.in);


String password;
System.out.print("Password:");

password = input.nextLine();

if (password.equals("president") == false )//can change password depending on the user
return;

else{
	
int selection2 = 1;
String query = null;//had to initialize as null to fix an error


while (selection2 != 0)
{
	System.out.println("Select Regular Member Actions(1) or Executive Actions(2):");
	int actionSelection;
	actionSelection = input.nextInt();
	
	if (actionSelection == 1)
		Member();
	
	else{
	/*System.out.println("Select what you would like to do:\n");
	System.out.println("Regular Member Actions:");
	System.out.print("See Club Information(1) \nView Club Events(2)\nView Future Events(3) \nView Contact List(4)\nCheck Dues Payment(5)\nCheck Status(6)\n\n");
	*/
	System.out.println("Executive Actions: ");
	System.out.print("Create New Member Contact(1) \nUpdate Contact List(2) \nDelete Member off Contact List(3) \nEdit Event Info(4) \nView Budgets(5) \nView Budgets for a Specific Month(6) \nView Total of Distinct budget categories(7) \nView Event Budgets(8)");
	
	System.out.print("Exit (0)\n");//Put in options for all of the use cases
	
	
	
	selection2 = input.nextInt();
	if (selection2 == 1){
		
		String c_name = null;
		String c_phonenum = null;
		String c_email = null;
		String c_position = null;
		int c_id;
		
		String dummy = input.nextLine();
		
		System.out.print("Enter their full name: ");
		c_name = input.nextLine();
		
		System.out.print("Enter their Phone number(Format:123-456-7892): ");
		c_phonenum = input.nextLine();
		
		System.out.print("Enter their email address: ");
		c_email = input.nextLine();
		
		System.out.print("Input their position, if any: ");
		c_position = input.nextLine();
		
		
		//idNumberCheck();
		
		System.out.print("Input their id Number: ");
		c_id = input.nextInt();
		
		
		
		query = "INSERT INTO Contact_List (c_name, c_phonenum, c_email, c_position, c_id) VALUES ('"+ c_name + "', '" +c_phonenum +"', '"+c_email+"', '"+c_position+"', "+c_id+") ;";//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
	
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			 stat.executeUpdate(query);
			

	       // ResultSet rs = stat.executeQuery(query);
	           
			
	        }
		
	
		catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
		
	}
	else if (selection2 == 2){

		String dummy2 = input.nextLine();
		System.out.print("Please enter the name of the member whose information you would like to change:");
		String inName= input.nextLine();
		System.out.print("Would you like to update phone number(c_phonenum), email(c_email), or position(c_position)?");
		String desUpdate=input.nextLine();
		System.out.print("What should the new value be?");
		String newValue=input.nextLine();
		
		query="Update Contact_List Set "+desUpdate+" = '"+newValue +"'Where c_name= '"+inName+"';";
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			stat.executeUpdate(query); 
			
			//ResultSet rs = stat.executeQuery(query);
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
		
	}	
	else if (selection2 == 3){
		
		String dummy3 = input.nextLine();
		System.out.print("Please enter the name of the person you would like to delete:");
		String inputID = input.nextLine();
		
		query="Delete From Contact_List Where c_name= '"+inputID + "'";
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			stat.executeUpdate(query); 
		
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
		
	}
	else if (selection2 == 4){
		
		String dummy2 = input.nextLine();
		System.out.print("Please enter the name of the event whose information you would like to change:");
		String inName= input.nextLine();
		System.out.print("Would you like to update location(ep_location), time(ep_when), setup time(ep_setupTime), contact member(ep_contact), or Materials needed(ep_materials)?");
		String desUpdate=input.nextLine();
		System.out.print("What should the new value be?");
		String newValue=input.nextLine();
		
		query="Update Event_Planning Set "+desUpdate+" = '"+newValue +"'Where ep_name= '"+inName+"';";
		
		
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			stat.executeUpdate(query);
			

	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
		
	}
	else if(selection2==5) {
		query = "Select * FROM Accounts";//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
	
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			ResultSet rs = stat.executeQuery(query);
			

	        while(rs.next()){
	           //Retrieve by column name
	           String category  = rs.getString("a_category");
	           
	           String balance = rs.getString("a_balance");
	           
	           String month = rs.getString("a_month");

	           //Display values
	           System.out.print("Category: " + category + "\n");
	           System.out.print("Balance: " + balance + "\n");
	           System.out.print("Month: " + month + "\n\n");			
	        }
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
	}	
	else if(selection2==6) {
			String dummy4 = input.nextLine();
			System.out.print("Please enter month in the form YYYY-MM:");
			String inMonth= input.nextLine();
			query = "Select * FROM Accounts WHERE a_month LIKE '"+inMonth+"%'";//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
	
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			ResultSet rs = stat.executeQuery(query);
			

	        while(rs.next()){
	           //Retrieve by column name
	           String category  = rs.getString("a_category");
	           
	           String balance = rs.getString("a_balance");
	           
	           String month = rs.getString("a_month");

	           //Display values
	           System.out.print("Category: " + category + "\n");
	           System.out.print("Balance: " + balance + "\n");
	           System.out.print("Month: " + month + "\n\n");			
	        }
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
	}	
	else if(selection2==7) {
		
		query = "Select a_category, SUM(a_balance) FROM Accounts Group By a_category";//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
	
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			ResultSet rs = stat.executeQuery(query);
			

	        while(rs.next()){
	           //Retrieve by column name
	           String category  = rs.getString("a_category");
	           
	           String balance = rs.getString("SUM(a_balance)");

	           //Display values
	           System.out.print("Category: " + category + "\n");
	           System.out.print("Balance: " + balance + "\n\n");
			
	        }
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
	}
	else if(selection2==8) {
		
		query = "Select * From Event_Budget";//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
	
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			ResultSet rs = stat.executeQuery(query);
			

	        while(rs.next()){
	           //Retrieve by column name
	           String category  = rs.getString("eb_name");
	           
	           String name = rs.getString("eb_category");
	           
	           String amount = rs.getString("eb_amount");

	           //Display values
	           System.out.print("name: " + name + "\n");
	           System.out.print("Category: " + category + "\n");
	           System.out.print("amount: " + amount + "\n\n");
			
	        }
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
	}

	
System.out.print("Would you like to do anything else? Yes(1) or No(0)\n");
selection2 = input.nextInt();
}
}
return;

}

}

public static void  EventPlanner() throws SQLException, ClassNotFoundException{
Connection conn = null;
Statement stat = null;
Class.forName("org.sqlite.JDBC");


Scanner input = new Scanner(System.in);


String password;
System.out.print("Password:");

password = input.nextLine();

if (password.equals("events") == false )//can change password depending on the user
return;

else{

int selection2 = 1;
String query = null;//had to initialize as null to fix an error
String query1 = null; String query2 = null;

while (selection2 != 0)
{
	System.out.println("Select what you would like to do:\n");
	System.out.print("Regular Member Actions(1) \nExecutive Member Actions(2)");
	int selection3=input.nextInt();
	if (selection3==1){
		Member();
		return;
	}
	else{
	System.out.println("Create an Event(1) \nDelete an Event(2)\nView Events(3)\nView only Future Events(4)\nAdd members to Attendance List(5) \nDelete from Attendance List(6)\nCreate an Event Budget(7) \nUpdate Event Budget(8) \nView attendance for member(9)\nChange participation status(10)\nView Event Budgets(11) \n");
	System.out.print("Exit (0)\n");
	
	selection2= input.nextInt();
	
	//Put in options for all of the use cases
	String dummy = input.nextLine();
	
	
	if (selection2 == 1){
		System.out.print("Name of event:");
		String inName=input.nextLine();
		System.out.print("Location:");
		String inLoc=input.nextLine();
		System.out.print("Start Time (YYYY-MM-DD HH:MM:SS):");
		String inWhen=input.nextLine();
		System.out.print("Setup Time (YYYY-MM-DD HH:MM:SS):");
		String inSetTime=input.nextLine();
		System.out.print("Contact Person:");
		String inCont=input.nextLine();
		System.out.print("Materials needed:");
		String inMat=input.nextLine();
		
		query1 = "Insert Into Event_Planning (ep_name, ep_location, ep_when, ep_setupTime, ep_contact, ep_materials) VALUES ( '"+inName+"','"+inLoc+"','"+inWhen+"','"+inSetTime+"','"+inCont+"','"+inMat+"');";
		
		query2 ="Insert Into Events (e_name, e_location, e_when) VALUES ('"+inName+"','"+inLoc+"','"+inWhen+"');";
	
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			stat.executeUpdate(query1);
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
		}
		
		
	try
	{
		conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
		stat = conn.createStatement();
		//stat.executeUpdate(query); 
		
		stat.executeUpdate(query2);
	

	}catch(SQLException se)
	{
		se.printStackTrace();
	}
	
	catch(Exception e)
	{
		e.printStackTrace();
	}
	
	finally
	{
		try
		{
			if(stat!=null)
				conn.close();
		}
		
		catch(SQLException se)
		{
		}

		try
		{
			if(conn!=null)
				conn.close();
		}
		
		catch(SQLException se)
		{
			se.printStackTrace();
		}
	}
	
	}

	
	if (selection2 == 2){
		System.out.print("Name of event you would like to delete: ");
		String inName=input.nextLine();
		
		
		query1 = "Delete  From Event_Planning Where ep_name = '"+ inName+ "';";
				
		
		query2 ="Delete  From Events Where e_name = '"+ inName+ "';";
	
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			stat.executeUpdate(query1);
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
		}
		
		
	try
	{
		conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
		stat = conn.createStatement();
		//stat.executeUpdate(query); 
		
		stat.executeUpdate(query2);
	

	}catch(SQLException se)
	{
		se.printStackTrace();
	}
	
	catch(Exception e)
	{
		e.printStackTrace();
	}
	
	finally
	{
		try
		{
			if(stat!=null)
				conn.close();
		}
		
		catch(SQLException se)
		{
		}

		try
		{
			if(conn!=null)
				conn.close();
		}
		
		catch(SQLException se)
		{
			se.printStackTrace();
		}
	}
	
	}
		
		else if (selection2 == 3){
			query=" Select * From Event_Planning";
			try
			{
				conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
				stat = conn.createStatement();
				//stat.executeUpdate(query); 
				
				ResultSet rs = stat.executeQuery(query);
				

		        while(rs.next()){
			           //Retrieve by column name
			           String e_name  = rs.getString("ep_name");
			           
			           String location = rs.getString("ep_location");
			           
			           String time = rs.getString("ep_when");
			           
			           String settime = rs.getString("ep_setupTime");
			           
			           String contact = rs.getString("ep_contact");
			           
			           String material = rs.getString("ep_materials");
			          

			           //Display values
			           System.out.print("Event Name: " + e_name + "\n");
			           System.out.print("Location: " + location + "\n");
			           System.out.print("Time of Event: " + time + "\n");
			           System.out.print("Time of setup: " + settime + "\n");
			           System.out.print("Contact: " + contact + "\n");
			           System.out.print("Materials Needed: " + material + "\n\n");
		           
				
		        }
			
		
			}catch(SQLException se)
			{
				se.printStackTrace();
			}
			
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			finally
			{
				try
				{
					if(stat!=null)
						conn.close();
				}
				
				catch(SQLException se)
				{
				}

				try
				{
					if(conn!=null)
						conn.close();
				}
				
				catch(SQLException se)
				{
					se.printStackTrace();
				}
		}
			
		}
		
		else if (selection2 == 4){
			query=" Select * From Event_Planning Where ep_when > date('now')";
			try
			{
				conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
				stat = conn.createStatement();
				//stat.executeUpdate(query); 
				
				ResultSet rs = stat.executeQuery(query);
				

		        while(rs.next()){
		           //Retrieve by column name
		           String e_name  = rs.getString("ep_name");
		           
		           String location = rs.getString("ep_location");
		           
		           String time = rs.getString("ep_when");
		           
		           String settime = rs.getString("ep_setupTime");
		           
		           String contact = rs.getString("ep_contact");
		           
		           String material = rs.getString("ep_materials");
		          

		           //Display values
		           System.out.print("Event Name: " + e_name + "\n");
		           System.out.print("Location: " + location + "\n");
		           System.out.print("Time of Event: " + time + "\n");
		           System.out.print("Time of setup: " + settime + "\n");
		           System.out.print("Contact: " + contact + "\n");
		           System.out.print("Materials Needed: " + material + "\n\n");
		        }
			
		
			}catch(SQLException se)
			{
				se.printStackTrace();
			}
			
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			finally
			{
				try
				{
					if(stat!=null)
						conn.close();
				}
				
				catch(SQLException se)
				{
				}

				try
				{
					if(conn!=null)
						conn.close();
				}
				
				catch(SQLException se)
				{
					se.printStackTrace();
				}
		}
			
		}
	
		else if (selection2 == 5){
			
				
				String at_membName = null;
				String at_eventName = null;
				int at_id;
				
				
				
				System.out.print("Enter their full name: ");
				at_membName = input.nextLine();
				
				System.out.print("Enter the event name: ");
				at_eventName = input.nextLine();
				
				
				
				
				
				
				
				
				query = "INSERT INTO Attendance (at_membName, at_eventName, at_id) VALUES ('"+ at_membName + "', '" +at_eventName +"',(Select c_id From Contact_List Where c_name = '" + at_membName + "') ) ;";//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
			
				try
				{
					conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
					stat = conn.createStatement();
					//stat.executeUpdate(query); 
					
					 stat.executeUpdate(query);
					

			       // ResultSet rs = stat.executeQuery(query);
			           
					
			        }
				
			
				catch(SQLException se)
				{
					se.printStackTrace();
				}
				
				catch(Exception e)
				{
					e.printStackTrace();
				}
				
				finally
				{
					try
					{
						if(stat!=null)
							conn.close();
					}
					
					catch(SQLException se)
					{
					}

					try
					{
						if(conn!=null)
							conn.close();
					}
					
					catch(SQLException se)
					{
						se.printStackTrace();
					}
			}
				
			}
	
		else if (selection2 == 6){
			
			
			String at_membName = null;
			String at_eventName = null;
			
			
			
			System.out.print("Enter the full name of the member you are taking off the attendance list: ");
			at_membName = input.nextLine();
			
			System.out.print("Enter the event name: ");
			at_eventName = input.nextLine();
			
			
			
			
			
			
			
			
			
			
			query = "Delete From Attendance Where at_membName = '"+ at_membName + "' AND at_eventName =  '" +at_eventName +"'  ;";//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
		
			try
			{
				conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
				stat = conn.createStatement();
				//stat.executeUpdate(query); 
				
				 stat.executeUpdate(query);
				

		       // ResultSet rs = stat.executeQuery(query);
		           
				
		        }
			
		
			catch(SQLException se)
			{
				se.printStackTrace();
			}
			
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			finally
			{
				try
				{
					if(stat!=null)
						conn.close();
				}
				
				catch(SQLException se)
				{
				}

				try
				{
					if(conn!=null)
						conn.close();
				}
				
				catch(SQLException se)
				{
					se.printStackTrace();
				}
		}
			
		}
	
		else if (selection2 == 7){
			

			System.out.print("Name of event:");
			String inName=input.nextLine();
			System.out.print("Category the Money is going into/coming from:");
			String inCat=input.nextLine();
			System.out.print("Amount of money: ");
			String inAmount=input.nextLine();
			
			
			query1 = "Insert Into Event_Budget (eb_name, eb_category, eb_amount) VALUES ( '"+inName+"','"+inCat+"','"+inAmount+"');";
			
			
			try
			{
				conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
				stat = conn.createStatement();
				//stat.executeUpdate(query); 
				
				stat.executeUpdate(query1);
			
		
			}catch(SQLException se)
			{
				se.printStackTrace();
			}
			
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			finally
			{
				try
				{
					if(stat!=null)
						conn.close();
				}
				
				catch(SQLException se)
				{
				}

				try
				{
					if(conn!=null)
						conn.close();
				}
				
				catch(SQLException se)
				{
					se.printStackTrace();
				}
			}
			
		}
		
		else if (selection2 == 8){
			

			String dummy2 = input.nextLine();
			System.out.print("Please enter the name of the Event whose budget you would like to change:");
			String inName= input.nextLine();
			
			String desUpdate="eb_amount";
			System.out.print("What should the new value be?");
			int newValue=input.nextInt();
			
			query="Update Event_Budget Set "+desUpdate+" = '"+newValue +"'Where eb_name= '"+inName+"';";
			try
			{
				conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
				stat = conn.createStatement();
				stat.executeUpdate(query); 
				
				//ResultSet rs = stat.executeQuery(query);
		
			}catch(SQLException se)
			{
				se.printStackTrace();
			}
			
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			finally
			{
				try
				{
					if(stat!=null)
						conn.close();
				}
				
				catch(SQLException se)
				{
				}

				try
				{
					if(conn!=null)
						conn.close();
				}
				
				catch(SQLException se)
				{
					se.printStackTrace();
				}
		}
			
		}	
	
else if (selection2 == 9){
			
	String at_membName = null;
	
	
	
	
	System.out.print("Enter the full name of the member you are taking off the attendance list: ");
	at_membName = input.nextLine();
	
	
	query = "Select at_membName, at_eventName From Attendance Where at_membName = '" + at_membName + "'";//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
	
	try
	{
		conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
		stat = conn.createStatement();
		//stat.executeUpdate(query); 
		
		ResultSet rs = stat.executeQuery(query);
		

        while(rs.next()){
           //Retrieve by column name
           String at_name  = rs.getString("at_membName");
           
           String event = rs.getString("at_eventName");
           
           
          

           //Display values
           System.out.print("Name: " + at_name + "\n");
           System.out.print("Event: " + event + "\n\n");
           
           
		
        }
	

	}catch(SQLException se)
	{
		se.printStackTrace();
	}
	
	catch(Exception e)
	{
		e.printStackTrace();
	}
	
	finally
	{
		try
		{
			if(stat!=null)
				conn.close();
		}
		
		catch(SQLException se)
		{
		}

		try
		{
			if(conn!=null)
				conn.close();
		}
		
		catch(SQLException se)
		{
			se.printStackTrace();
		}
}
	
}

else if (selection2 == 10){
	//String dummy = input.nextLine();
	System.out.print("Please enter the name of the member you would like to affect:");
	String inName=input.nextLine();
	System.out.print("Do they have Good or Bad standing:");
	String inStand=input.nextLine();
	query = "Update Status Set s_part ='"+inStand+"' Where s_id= (Select c_id From Contact_List Where c_name= '"+inName+"')";//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 

	try
	{
		conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
		stat = conn.createStatement();
		stat.executeUpdate(query); 
		
	}catch(SQLException se)
	{
		se.printStackTrace();
	}
	
	catch(Exception e)
	{
		e.printStackTrace();
	}
	
	finally
	{
		try
		{
			if(stat!=null)
				conn.close();
		}
		
		catch(SQLException se)
		{
		}

		try
		{
			if(conn!=null)
				conn.close();
		}
		
		catch(SQLException se)
		{
			se.printStackTrace();
		}
}
	
}
	
	
else if (selection2 == 11){
	query = "Select * From Event_Budget";//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
	
	try
	{
		conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
		stat = conn.createStatement();
		//stat.executeUpdate(query); 
		
		ResultSet rs = stat.executeQuery(query);
		

        while(rs.next()){
           //Retrieve by column name
           String category  = rs.getString("eb_name");
           
           String name = rs.getString("eb_category");
           
           String amount = rs.getString("eb_amount");

           //Display values
           System.out.print("name: " + name + "\n");
           System.out.print("Category: " + category + "\n");
           System.out.print("amount: " + amount + "\n\n");
		
        }
	

	}catch(SQLException se)
	{
		se.printStackTrace();
	}
	
	catch(Exception e)
	{
		e.printStackTrace();
	}
	
	finally
	{
		try
		{
			if(stat!=null)
				conn.close();
		}
		
		catch(SQLException se)
		{
		}

		try
		{
			if(conn!=null)
				conn.close();
		}
		
		catch(SQLException se)
		{
			se.printStackTrace();
		}
}
}

	
	}	
	System.out.print("Would you like to do anything else? Yes(1) or No(0)\n");
	selection2= input.nextInt();
}	
	
	return;
}
}
	


public static void  Treasurer() throws SQLException, ClassNotFoundException{
Connection conn = null;
Statement stat = null;
Class.forName("org.sqlite.JDBC");


Scanner input = new Scanner(System.in);


String password;
System.out.print("Password:");

password = input.nextLine();

if (password.equals("treasurer") == false )//can change password depending on the user
return;

else{

int selection2 = 1;
String query = null;//had to initialize as null to fix an error


while (selection2 != 0)
{
	System.out.println("Select what you would like to do:\n");
	System.out.print("Regular Member Actions(1) \nExecutive Member Actions(2)");
	int selection3=input.nextInt();
	if (selection3==1){
		Member();
		return;
	}
	else{
	System.out.println("Update Dues(1) \nUpdate Status(2)\n Update Event Budgets(3)\nView Event Budgets(4)\nView Budget Totals(5)");
	
	System.out.print("Exit (0)\n");//Put in options for all of the use cases
	
	
	selection2 = input.nextInt();
	if (selection2 == 1){
		String dummy = input.nextLine();
		System.out.print("Please enter the name of the member you would like to affect:");
		String inName=input.nextLine();
		System.out.print("Please enter the amount they paid:");
		int inDues=input.nextInt();
		query = "Update DUES Set d_amount ="+inDues+" Where d_id= (Select c_id From Contact_List Where c_name= '"+inName+"')";//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
	
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			stat.executeUpdate(query);
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
		
	}
	else if (selection2 == 2){
		String dummy = input.nextLine();
		System.out.print("Please enter the name of the member you would like to affect:");
		String inName=input.nextLine();
		System.out.print("Do they have Good or Bad standing:");
		String inStand=input.nextLine();
		query = "Update Status Set s_dues ='"+inStand+"' Where s_id= (Select c_id From Contact_List Where c_name= '"+inName+"')";//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
	
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			stat.executeUpdate(query); 
			
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
		
	}
	else if (selection2 == 3){
		

			String dummy2 = input.nextLine();
			System.out.print("Please enter the name of the Event whose budget you would like to change:");
			String inName= input.nextLine();
			
			String desUpdate="eb_amount";
			System.out.print("What should the new value be?");
			int newValue=input.nextInt();
			
			query="Update Event_Budget Set "+desUpdate+" = '"+newValue +"'Where eb_name= '"+inName+"';";
			try
			{
				conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
				stat = conn.createStatement();
				stat.executeUpdate(query); 
				
				//ResultSet rs = stat.executeQuery(query);
		
			}catch(SQLException se)
			{
				se.printStackTrace();
			}
			
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			finally
			{
				try
				{
					if(stat!=null)
						conn.close();
				}
				
				catch(SQLException se)
				{
				}

				try
				{
					if(conn!=null)
						conn.close();
				}
				
				catch(SQLException se)
				{
					se.printStackTrace();
				}
		}
			
		}	
		
	
	else if (selection2 == 4){
		query = "Select * From Event_Budget";//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
		
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			ResultSet rs = stat.executeQuery(query);
			

	        while(rs.next()){
	           //Retrieve by column name
	           String category  = rs.getString("eb_name");
	           
	           String name = rs.getString("eb_category");
	           
	           String amount = rs.getString("eb_amount");

	           //Display values
	           System.out.print("name: " + name + "\n");
	           System.out.print("Category: " + category + "\n");
	           System.out.print("amount: " + amount + "\n\n");
			
	        }
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
	}
	
	else if (selection2 == 5){
		
		query = "Select a_category, a_balance From Accounts Group By a_category";//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
	
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Larrys 5010 Dell/Documents/Back-up Dead Laptop/Repository/universal-organization-management-database-system/Database_Project.db");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			ResultSet rs = stat.executeQuery(query);
			

	        while(rs.next()){
	           //Retrieve by column name
	           String category  = rs.getString("a_category");
			   String balance  = rs.getString("a_balance");
	          
			  //Display values
	           System.out.print("Category: " + category + "\n");
			   System.out.print("Amount Paid: " + balance + "\n\n");
			
	        }
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
	}
		
	}
}	
	
System.out.print("Would you like to do anything else? Yes(1) or No(0)\n");
selection2= input.nextInt();
}
return;



}






/*public static void idNumberCheck () throws SQLException, ClassNotFoundException{
Connection conn = null;
Statement stat = null;
Class.forName("org.sqlite.JDBC");


Scanner input = new Scanner(System.in);

int maxID;
String query = null;//had to initialize as null to fix an error



	
		
		query = "Select Max(c_id From Contact_List";//insert the SQL statements. For some of them, like dues, we can ask for the input of the id number as a string and use + to put it in sql 
	
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Kevin Cunha/workspace/CSE_111_Project/Database_Project");//insert database location here
			stat = conn.createStatement();
			//stat.executeUpdate(query); 
			
			ResultSet rs = stat.executeQuery(query);
			

	        while(rs.next()){
	           //Retrieve by column name
	           maxID  = rs.getInt("c_id");
	           
	           

	           //Display values
	           System.out.print("New ID to input : " + maxID + 1 + "\n");
	         
	           
	          
	        }
		
	
		}catch(SQLException se)
		{
			se.printStackTrace();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			try
			{
				if(stat!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
			}

			try
			{
				if(conn!=null)
					conn.close();
			}
			
			catch(SQLException se)
			{
				se.printStackTrace();
			}
			
			
	}
		
	
	
	
	
	
	
	
	

	
	
	return;


}


	*/
	
	


}
}

//}
//}
//copy method for each user
//}